# -*- coding: utf-8 -*-
import sys

import os
import re
from celery import Celery
from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_sslify import SSLify

reload(sys)
sys.setdefaultencoding("UTF-8")

flask_app = Flask(__name__)
flask_app.config.from_object('config.default')
if 'APP_CONFIG_FILE' in os.environ:
    flask_app.config.from_envvar('APP_CONFIG_FILE')
if 'APP_CONFIG_PYFILE' in os.environ:
    flask_app.config.from_pyfile(os.environ['APP_CONFIG_PYFILE'])

db = SQLAlchemy(flask_app)
sslify = SSLify(flask_app)

celery_app = Celery('tasks', broker=flask_app.config["AMQP_URL"])
celery_app.conf.update(flask_app.config)

lm = LoginManager()
lm.init_app(flask_app)
lm.login_view = 'login'
lm.login_message = u"Необходима авторизация"
lm.login_message_category = 'info'  # 'warning'

if flask_app.config['PROFILE']:
    from werkzeug.contrib.profiler import ProfilerMiddleware

    flask_app.wsgi_app = ProfilerMiddleware(flask_app.wsgi_app, restrictions=[50])

from flask_assets import Environment, Bundle

assets = Environment(flask_app)
css = Bundle('bootstrap.min.css', 's.css', filters="cssmin", output="packed.css")
# js     = Bundle('moment-with-locales.min.js',
# 'jquery-1.11.2.min.js',
# 'bootstrap.min.js',
# 'pay_validate.js',
#  filters="rjsmin",
#  output="packed.js")
# ,'moment.min.js'
# assets.register('js_all', js)
assets.register('css_all', css)

# from flask.ext.cache import Cache
# cache = Cache(flask_app,config={'CACHE_TYPE': 'simple'}) # Check Configuring Flask-Cache section for more details
if flask_app.config.get('USE_CFFI', False):
    try:
        import psycopg2
    except ImportError:
        # Fall back to psycopg2-ctypes
        # from psycopg2ct import compat
        # compat.register()
        from psycopg2cffi import compat

        compat.register()

from flask_mail import Mail

# from app.mymail import Mail, Message
mail = Mail(flask_app)

# Установим переменные
r = re.search('(?P<protocol>.*?)://(?P<user>.*?):(?P<pass>.*?)@(?P<host>.*?):(?P<port>.*?)/(?P<db>.*)',
              flask_app.config['DB_URL'])
flask_app.config['DB_PROTOCOL'], flask_app.config['DB_USER'], flask_app.config['DB_PASS'], flask_app.config['DB_HOST'], \
flask_app.config['DB_PORT'], flask_app.config['DB_NAME'] = r.group('protocol'), r.group('user'), r.group(
    'pass'), r.group('host'), r.group('port'), r.group('db')

from app.views import *
from app.jinja_globals import *
