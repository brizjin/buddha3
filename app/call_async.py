# -*- coding: utf-8 -*-
import threading,traceback#,thread
import datetime,time
class timer(object):
	def __init__(self):
		self.begin = datetime.datetime.now()
	def interval(self):
		#return (datetime.datetime.now() - self.begin).seconds
		delta = datetime.datetime.now() - self.begin
		delta = float(delta.seconds) + float(delta.microseconds)/1000000
		return delta
def call_async(call_func,on_complete=None,msg=None,async_callback=False):
	class ThreadProgress():
	    def __init__(self, thread, message):
	        self.thread = thread
	        self.message = message	        
	        self.addend = 1
	        self.size = 8
	        self.d_begin = datetime.datetime.now()
	        sublime.set_timeout(lambda: self.run(0), 100)

	    def run(self, i):
	        if not self.thread.is_alive():
	            if hasattr(self.thread, 'result') and not self.thread.result:
	                sublime.status_message('')
	                return
	            delta = datetime.datetime.now() - self.d_begin
	            delta_str = (datetime.datetime.min + delta).time().strftime("%H:%M:%S.%f").strip("0:")            
	            how_long = unicode("Выполненно за %s сек" % delta_str,'utf-8')
	            how_long = self.message + u". " + how_long if self.message else how_long
	            sublime.status_message(how_long)
	            return

	        before = i % self.size
	        after = (self.size - 1) - before

	        sublime.status_message('%s [%s=%s]' % \
	        	(self.message, u' ' * before, ' ' * after))
	            #(unicode(self.message,'utf-8'), ' ' * before, ' ' * after))

	        if not after:
	            self.addend = -1
	        if not before:
	            self.addend = 1
	        i += self.addend

	        sublime.set_timeout(lambda: self.run(i), 100)
	class RunInThread(threading.Thread):
		def run(self):
			try:
				if msg:
					ThreadProgress(self,msg)
				t = timer()
				self.result = call_func()
				# def prn():
				# 	import inspect
				# 	print u'%s за %s,%s'%(msg,t.interval(),inspect.getsource(call_func))
				# sublime.set_timeout(prn, 0)
				if on_complete:
					def on_done():
						#t = timer()
						if self.result == None:
							on_complete()
						elif self.result.__class__ == tuple:
							on_complete(*self.result)
						else:
							on_complete(self.result)
						#t.print_time(msg + ' ' + "call_async on_complete:" + on_complete.__name__)
					if async_callback:
						on_done()
					else:
						sublime.set_timeout(on_done, 0)
			except Exception,e:
				print u"*** Ошибка асинхронного вызова:",e
				import inspect
				print u"*** CALLBACK STACK:",traceback.format_stack(),'\n',inspect.getsource(call_func)
				# print "*** При вызове ",call_func.im_class if hasattr(call_func,"im_class") else "",call_func.__name__
				# if sys != None:
				# 	exc_type, exc_value, exc_traceback = sys.exc_info()
					
				# 	traceback.print_exception(exc_type, exc_value, exc_traceback,
				# 		                          limit=10, file=sys.stdout)
				
				
	th = RunInThread(on_complete)
	th.start()
	return th