# -*- coding: utf-8 -*-

from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import DataRequired, Required, InputRequired


class OrderAddForm(FlaskForm):
    summa = DecimalField(u"Сумма пожертвований:",  validators=[DataRequired()])
    project = RadioField(u'Проекты',  coerce=int, validators=[DataRequired()])
    subs = RadioField(u'Подписка', coerce=int, choices=[(1, u'Без подписки'),
                                                        (2, u'Три месяца'),
                                                        (3, u'Пол года'),
                                                        (4, u'Год'),
                                                        (5, u'Три года')], default=1)


class ProjectForm(FlaskForm):
    name = StringField(u"Название проекта", validators=[DataRequired()])
    # caption = StringField(u"Описание проекта", validators=[DataRequired()])
    name_en = StringField(u"Английское название", validators=[DataRequired()])


class LoginForm(FlaskForm):
    form_header = u"Представьтесь, пожалуйста"
    email = StringField(u"Введите email", validators=[DataRequired()], description={"autofocus": True})
    password = PasswordField(u"Введите пароль", validators=[DataRequired()])
    submit = SubmitField(u"Войти", description={"class": "btn btn-success"})


class RegisterForm(FlaskForm):
    form_header = u"Регистрация нового пользователя"
    email = StringField(u"Введите e-mail", validators=[InputRequired()], description={"autofocus": True})
    # username = StringField(u"Введите логин", validators=[DataRequired()], description={"autofocus": True})
    password = PasswordField(u"Введите пароль", validators=[DataRequired()])
    phone = StringField(u"Введите телефон в формате: +79151234567", validators=[DataRequired()])
    submit = SubmitField(u"Зарегистрироваться", description={"class": "btn btn-success"})


class IniteRegForm(FlaskForm):
    form_header = u"Регистрация"
    username = StringField(u"Введите e-mail", validators=[DataRequired()], 	description={"autofocus": True})
    password = PasswordField(u"Введите пароль", validators=[DataRequired()])
    phone = StringField(u"Телефон в формате +79123456789", validators=[DataRequired()])
    submit = SubmitField(u"Зарегистрироваться", description={"class": "btn btn-success"})


class EmailToRenewPasswordForm(FlaskForm):
    form_header = u"Восстановление пароля"
    email = StringField(u"E-mail", validators=[DataRequired()], description={"autofocus": True})
    submit = SubmitField(u"Восстановить", description={"class": "form-control btn btn-success"})


class SendInviteForm(FlaskForm):
    form_header = u"Отправить приглашение"
    email = StringField(u"Введите e-mail", validators=[DataRequired()], description={"autofocus": True})
    submit = SubmitField(u"Отправить", description={"class": "form-control btn btn-success"})


class CreateNewPass(FlaskForm):
    form_header = u"Смена пароля"
    new_pass = PasswordField(u"Введите новый пароль", validators=[DataRequired()], description={"autofocus": True})
    new_pass_again = PasswordField(u"Введите повторно новый пароль", validators=[DataRequired()])
    submit = SubmitField(u"Сменить", description={"class": "form-control btn btn-success"})


class DatePeriodFilterForm(FlaskForm):
    date_beg = DateTimeField(u"Дата начала", format='%d.%m.%Y')
    date_end = DateTimeField(u"Дата окончания", format='%d.%m.%Y')
