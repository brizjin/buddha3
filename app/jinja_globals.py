# -*- coding: utf-8 -*-
import datetime

from app import flask_app
from flask import url_for
from momentjs import momentjs
import os


def date_format_long(value):
    if value:
        month = {1: u'Января', 2: u'Февраля', 3: u'Марта', 4: u'Апреля', 5: u'Мая', 6: u'Июня', 7: u'Июля',
                 8: u'Августа', 9: u'Сентября', 10: u'Октября', 11: u'Ноября', 12: u'Декабря', }[value.month]
        return ('%02d %s %s' % (value.day, month, value.year)).lower()


def date_month(value):
    if value:
        month = {1: u'Январь', 2: u'Февраль', 3: u'Март', 4: u'Апрель', 5: u'Май', 6: u'Июнь', 7: u'Июль', 8: u'Август',
                 9: u'Сентябрь', 10: u'Октябрь', 11: u'Ноябрь', 12: u'Декабрь', }[value.month]
        return '%s %s' % (month, value.year)


def format_column(value, column_type=''):
    if column_type == 'month':
        return date_month(value)
    elif isinstance(value, datetime.datetime):
        return date_format_long(value)
    else:
        return value

flask_app.jinja_env.globals.update(date_format_long=date_format_long)
flask_app.jinja_env.globals.update(date_month=date_month)
flask_app.jinja_env.globals.update(is_debug=flask_app.debug)
flask_app.jinja_env.globals.update(db_name=flask_app.config["DB_URL"])
flask_app.jinja_env.globals.update(format_column=format_column)

flask_app.jinja_env.globals['momentjs'] = momentjs


@flask_app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)


def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(flask_app.root_path,
                                     endpoint, filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)
