﻿import datetime

from app import db


# class User(db.Model):
# 	id 		= db.Column(db.Integer, primary_key=True)
# 	name 	= db.Column(db.String(80))
# 	email	= db.Column(db.String(120),unique=True)

# 	def __init__(self,name,email):
# 		self.name  = name
# 		self.email = email

# 	def __repr__(self):
# 		return 'user(%s,%s)' % (self.name,self.email)


class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    name_en = db.Column(db.String(80))
    caption = db.Column(db.String(2000))
    orders = db.relationship('Order', backref='project', lazy='dynamic')
    date_beg = db.Column(db.DateTime, default=datetime.datetime.utcnow())
    date_end = db.Column(db.DateTime)

    # def __init__(self, name, caption):
    #     self.name = name
    #     self.caption = caption

    def __repr__(self):
        return 'project(%s,%s)' % (self.name, self.caption)


class Order(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # payer 		= db.Column(db.String(80))
    project_id = db.Column(db.Integer, db.ForeignKey('project.id'))
    summa = db.Column(db.Float)
    status = db.Column(db.String(250))
    user_id = db.Column(db.Integer)
    date_beg = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    sub_id = db.Column(db.Integer)
    date_for = db.Column(db.DateTime)
    status_up = db.Column(db.Integer, default=0)
    recommendation = db.Column(db.String(2000))
    message = db.Column(db.String(2000))
    email = db.Column(db.String(2000))
    cardholder = db.Column(db.String(2000))

    def __repr__(self):
        return 'order(%s,%s)' % (self.project_id, self.summa)

    @property
    def project(self):
        return db.session.query(Project).get(self.project_id)

    @property
    def user(self):
        if self.user_id:
            # print "USERID=",self.user_id
            return db.session.query(User).get(self.user_id)
        else:
            return None

    @property
    def sub(self):
        if self.sub_id:
            return db.session.query(Sub).get(self.sub_id)


class Status(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(2000))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return 'status(%s,%s)' % (self.id, self.name)


class Sub(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_beg = db.Column(db.DateTime, default=datetime.datetime.utcnow)  # datetime.datetime.utcnow()
    # date_next     = db.Column(db.DateTime, default = datetime.datetime.now()  + datetime.timedelta(days=1))
    date_end = db.Column(db.DateTime)
    months_to_end = db.Column(db.Integer)
    first_order_id = db.Column(db.Integer)
    # user_id  	  = db.Column(db.Integer)
    # order_last_id = db.Column(db.Integer)
    # active 		  = db.Column(db.SmallInteger, default = 1)

    # @property
    # def user(self):
    # 	if self.user_id:
    # 		return db.session.query(User).get(self.user_id)
    # 	else:
    # 		return None

    # last_order    = None
    # @property
    # def order(self):
    # 	if self.last_order:
    # 		#print "GET ORDER from cache"
    # 		return self.last_order
    # 	else:
    # 		#print "GET ORDER new"
    # 		if self.order_last_id:
    # 			self.last_order = db.session.query(Order).get(self.order_last_id)
    # 			return self.last_order
    # 		else:
    # 			return None

    # @property
    # def is_active(self):
    # 	return self.active == 1


ROLE_USER = 0
ROLE_ADMIN = 1


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # username = db.Column(db.String(64), unique=True)
    # userpassword = db.Column(db.String(64))
    mailpassword = db.Column(db.String(64))
    email = db.Column(db.String(120), index=True)
    role = db.Column(db.SmallInteger, default=ROLE_USER)
    phone = db.Column(db.String(64))
    date_reg = db.Column(db.DateTime, default=datetime.datetime.utcnow)
    # posts = db.relationship('Post', backref = 'author', lazy = 'dynamic')
    is_notify = db.Column(db.Boolean, default=True)

    @property
    def is_admin(self):
        if self.role == ROLE_ADMIN:
            return True
        else:
            return False

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return self.id

    def __repr__(self):
        return '<User %r>' % self.username


class Log(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    text = db.Column(db.String(32000))
