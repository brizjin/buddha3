select a.*
	,case when a.status = 'paid' then 'Оплачена'
		  when a.status = 'active' then 'Активна'
		  when a.status = 'stoped' then 'Остановлена'
	end status_name
from (
	select a.*
		, case
			when a.date_end is not null then 'stoped'
			when a.sum_payed >= sum_total then 'paid'
			when a.date_end is null and a.first_order_status in ('paid', 'authorized') then 'active'
		  end status
	from (
		select s.id																	sub_id
			, 'подписка №' || s.id													sub_n
			, ' на ' || p.name														sub_name
			, o.summa || 'р./месяц, в течении ' || s.months_to_end || ' месяцев'	sub_summa
			, s.date_beg															date_beg
			, s.date_end															date_end
			, s.months_to_end
			, o.id																	first_order_id
			, o.summa
			, o.user_id
			, o.status																first_order_status
			, u.email
			, u.phone
			, p.id project_id
			, p.name project_name
			,(SELECT sum(o.summa) FROM "order" o WHERE o.sub_id = s.id and o.status in ('paid', 'authorized')) sum_payed
			, o.summa * s.months_to_end sum_total

		from sub s
		left join "order" o on s.first_order_id = o.id
		left join "user" u on o.user_id = u.id
		left join "project" p on p.id = o.project_id
		--where s.date_end is null and o.status in ('paid', 'authorized')
		--where user_id = 1
	) a
) a
order by a.date_beg desc