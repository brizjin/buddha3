﻿--SELECT row_number() OVER (ORDER BY a.date_plan) n, sub_id, date_plan, date_paid, summa, email, phone, project_id, project_name, sum_payed, sum_plan, date_beg, months_to_end, user_id, first_order_id
SELECT *
from (
	select a.*
		,(SELECT b.date_beg
		  FROM (SELECT sum(o.summa) OVER (PARTITION BY o.sub_id ORDER BY o.date_beg, o.id) sum_plan, o.date_beg date_beg FROM "order" o WHERE o.sub_id = a.sub_id AND o.status IN ('paid', 'authorized')) b
		  WHERE b.sum_plan >= a.sum_plan
		  ORDER BY b.date_beg
		  LIMIT 1) date_paid
	from (
		select a.*
		     ,sum(a.summa) OVER (PARTITION BY a.sub_id ORDER BY d) 	sum_plan
		     ,d.d 							date_plan
		from (%s) a
		LEFT JOIN generate_series(date_trunc('day', a.date_beg),date_trunc('day', a.date_beg) + INTERVAL '1 month' * (a.months_to_end - 1),'1 month') d ON a.months_to_end > 0
    ) a
) a
where 1=1
  --and sub_id = 48
  --and date_paid is null
order by date_plan
