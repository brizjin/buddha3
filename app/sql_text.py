subs_to_pay = """
	SELECT
	     d.d for_date
	    ,s.*
	    ,(select count(1) from "order" o where o.sub_id = s.id and date_trunc('day',o.date_beg) = date_trunc('day',d.d) and o.status = 'paid') 	     paid_at_date
	    ,(select count(1) from "order" o where o.sub_id = s.id and date_trunc('day',o.date_beg) = date_trunc('day',d.d) and o.status = 'authorized') authorized_at_date
	FROM generate_series(:date_beg, :date_end, '1 day') d
	    ,subs_view s
	where 1=1
	  and(d.d <= s.date_end or s.date_end is null)
	  and d.d >  s.date_beg
	  and last_paid_order is not null
	order by d.d,s.date_beg
	;
"""