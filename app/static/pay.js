jQuery( document ).ready(function() {
  var form = jQuery('#uniteller_form')
  var prepare_form_func = function(event) {
    //console.log("uniteller_submit is clicked")
    event.preventDefault();    
    var summa      = parseInt(form.find('input[name=summa]').val())
    //var project_id = parseInt(form.find('input[type=radio]:checked').attr('id'))
    //console.log(project_id)
    var project_id = 1
    if (!summa || !$("#offert").prop("checked")){
      return
    }    
    $.getJSON('/sign', {
        summa       : summa,
        project_id  : project_id
      }, function(data) {
        form.find('input[name=Order_IDP]').val(data.order);
        form.find('input[name=Subtotal_P]').val(data.summa);
        form.find('input[name=Signature]').val(data.sign);
        form.submit();
      });
    };  
  jQuery("#uniteller_submit").click(prepare_form_func);
  //jQuery("#uniteller_form").submit(prepare_form_func2);

  $("#uniteller_submit").prop('disabled', !$("#offert").prop("checked"));
  jQuery("#offert").change(function(event) {
    $("#uniteller_submit").prop('disabled', !$(this).prop("checked"));
  });
});