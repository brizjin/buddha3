jQuery( document ).ready(function() {
  $("#uniteller_submit1").prop('disabled', !$("#offert1").prop("checked"));
  jQuery("#offert1").change(function(event) {
    $("#uniteller_submit1").prop('disabled', !$(this).prop("checked"));
  });
  $("#uniteller_submit2").prop('disabled', !$("#offert2").prop("checked"));
  jQuery("#offert2").change(function(event) {
      $("#uniteller_submit2").prop('disabled', !$(this).prop("checked"));
  });
});