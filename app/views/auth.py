# -*- coding: utf-8 -*-
from flask import get_flashed_messages
from sqlalchemy import and_, func

from app import forms
from app.forms import *
from flask import request, render_template, flash, redirect, url_for
from flask_login import login_user, logout_user, current_user, login_required
import app.models as models
from app import flask_app, db
from app.lib import *
from app.mymail import Message
from app import mail
import re

email_address = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")
phone_format = re.compile(r"\+\d{11,13}")


@flask_app.route('/registration', methods=['GET', 'POST'])
def register():
    pform = forms.RegisterForm()
    if request.method == 'GET':
        # if not pform.validate_on_submit():
        return render_template('base_dialog_form.html', form=pform)
    else:
        if pform.email.data == '':
            flash(u'Необходимо указать email', u'danger')
        elif not email_address.match(pform.email.data):
            flash(u'Не верный формат email', u'danger')
        if pform.password.data == '':
            flash(u'Необходимо указать пароль', u'danger')
        if pform.phone.data == '':
            flash(u'Необходимо указать телефон', u'danger')
        if not phone_format.match(pform.phone.data):
            flash(u'Не верный формат телефона. Необходимо указать телефон в формате +71234567890', u'danger')
        if db.session.query(models.User).filter_by(email=pform.email.data).all():
            flash(u'Пользователь с email %s уже существует' % request.form['email'], u'danger')
        if len(get_flashed_messages()) > 0:
            return render_template('base_dialog_form.html', form=pform)

        user = models.User()
        user.email = pform.email.data.strip().lower()
        user.phone = pform.phone.data.strip().lower()
        user.mailpassword = md5(user.email + pform.password.data)
        db.session.add(user)
        db.session.commit()

        flash(u'Пользователь %s успешно зарегистрован' % user.email, u'success')
        # return redirect(url_for('login'))
        login_user(user, remember=True)
        return redirect(request.args.get('next') or url_for('index'))


@flask_app.route('/login', methods=['GET', 'POST'])
def login():
    pform = LoginForm()
    # if not form.validate_on_submit():
    if request.method == 'GET':
        return render_template('login.html', form=pform)
    else:
        email = pform.email.data.strip().lower()
        password = md5(email + pform.password.data)
        registered_user = models.User.query.filter(func.lower(models.User.email) == email,
                                                   models.User.mailpassword == password).first()
        remember_me = True

        if registered_user is None:
            flash(u'Ошибка e-mail или пароля', 'danger')
            return redirect(url_for('login'))
        else:
            flash(u'Вы успешно залогинились', 'info')
        login_user(registered_user, remember=remember_me)
        return redirect(request.args.get('next') or url_for('index'))


@flask_app.route('/logout')
def logout():
    logout_user()
    flash(u'Вы успешно разлогинились', 'info')
    return redirect(url_for('index'))


@flask_app.route('/renewpassword', methods=['GET', 'POST'])
def renewpassword():
    pform = forms.EmailToRenewPasswordForm()
    if not pform.validate_on_submit():
        return render_template('base_dialog_form.html', form=pform)
    else:
        email = pform.email.data.strip().lower()
        user_to_renew = models.User.query.filter(func.lower(models.User.email) == email).first()
        if user_to_renew:
            msg = Message('subject', sender='hello@gmail.com', recipients=[user_to_renew.email])
            msg.html = u'Для восстановления пароля на <b>donate.buddhism.ru</b> перейдите по ' \
                       u'<a href="donate.buddhism.ru/newpass/%s/%s">ссылке</a>' % \
                       (user_to_renew.email.encode('utf8'),
                        hashlib.md5(user_to_renew.email).hexdigest())
            mail.send(msg)

            return render_template('show_message.html',
                                   msg_header=u"Восстановление пароля",
                                   msg_body=u"Вам на почтовый ящик была отправленна ссылка для восстановления пароля.")
        else:
            flash(u"Email %s не найден." % pform.email.data, 'danger')
            return redirect(request.args.get('next') or url_for('renewpassword'))


@flask_app.route('/newpass/<email>/<hashpass>', methods=['GET', 'POST'])
def newpass(email, hashpass):
    pform = forms.CreateNewPass()
    user_to_renew = models.User.query.filter(func.lower(models.User.email) == func.lower(email)).first()
    if not user_to_renew:
        flash(u"Пользователь не найден.", 'danger')
        return render_template('base_dialog_form.html', form=pform)
    if hashpass != hashlib.md5(user_to_renew.email).hexdigest():
        flash(u"Не верный проверочный код.", 'danger')
        return render_template('base_dialog_form.html', form=pform)

    # http://localhost:5000/newpass/admin/20d0b0e10cd873f158819c74ac66b1ac
    if not pform.validate_on_submit():
        return render_template('base_dialog_form.html', form=pform)
    else:
        if pform.new_pass.data != pform.new_pass_again.data:
            flash(u'Пароли не совпадают', 'danger')
            return render_template('base_dialog_form.html', form=pform)
        else:
            # user_to_renew.userpassword = hashlib.md5(user_to_renew.username + pform.new_pass.data).hexdigest()
            user_to_renew.mailpassword = hashlib.md5(
                user_to_renew.email.strip().lower() + pform.new_pass.data).hexdigest()
            db.session.commit()

            msg = Message('Пароль на donate.buddhism.ru был восстановлен',
                          sender='hello@gmail.com',
                          recipients=[user_to_renew.email])
            msg.html = u'''
                <h4>Вы успешно восстановили свой пароль на donate.buddhism.ru</h4>
                <table id="orders_table" class="table borderless" style="margin-bottom: 0px;">
                    <tr><td style="border: none;">Почта</td><td style="border: none;">{0}</td></tr>
                    <tr><td style="border: none;">Пароль</td><td style="border: none;">{1}</td></tr>
                </table>'''.format(user_to_renew.email, pform.new_pass.data)

            mail.send(msg)

            flash(u'Пароль успешно изменен', u'success')
            return redirect(request.args.get('next') or url_for('login'))


# @flask_app.route('/create_new_pass', methods=['GET', 'POST'])
# @login_required
# def create_new_pass():
#     pform = forms.CreateNewPass()
#     if not pform.validate_on_submit():
#         return render_template('base_dialog_form.html', form=form)
#     else:
#         if pform.new_pass.data != pform.new_pass_again.data:
#             flash(u'Пароли не совпадают', 'danger')
#             return render_template('base_dialog_form.html', form=form)
#         else:
#             user_to_renew = current_user
#
#             user_to_renew.userpassword = hashlib.md5(user_to_renew.username + pform.new_pass.data).hexdigest()
#             user_to_renew.mailpassword = hashlib.md5(user_to_renew.email + pform.new_pass.data).hexdigest()
#             db.session.commit()
#
#             msg = Message('Пароль на donate.buddhism.ru был восстановлен',
#                           sender='hello@gmail.com',
#                           recipients=[user_to_renew.email])
#             msg.html = u'''
#                 <h4>Вы успешно восстановили свой пароль на donate.buddhism.ru</h4>
#                 <table id="orders_table" class="table borderless" style="margin-bottom: 0px;">
#                     <tr><td style="border: none;">Почта</td><td style="border: none;">{0}</td></tr>
#                     <tr><td style="border: none;">Пароль</td><td style="border: none;">{1}</td></tr>
#                 </table>'''.format(user_to_renew.email, pform.new_pass.data)
#
#             mail.send(msg)
#
#             flash(u'Пароль успешно изменен', u'success')
#             return redirect(request.args.get('next') or url_for('login'))
