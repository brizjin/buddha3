﻿# -*- coding: utf-8 -*-
import datetime
import hashlib

from flask import render_template, session, request, url_for
from flask_login import current_user

import app.models
import app.models as models
from app import flask_app, db
from app.forms import *


def md5str(value):
    m = hashlib.md5()
    m.update(value.encode('utf8'))
    return str(m.hexdigest())


# def add_months(value, months_to_add=1):
#     newyear = value.year + (value.month + months_to_add)//12
#     newmonth = value.month + months_to_add - ((value.month + months_to_add)//12) * 12
#     # print 'M=',value.month ,months_to_add ,months_to_add//12 ,(months_to_add//12) * 12, newMonth
#     newdate = datetime.date(newyear, newmonth, value.day)
#     return newdate


@flask_app.route('/donate', methods=['GET', 'POST'])
def donate():
    return donate_render('donate.html')


@flask_app.route('/donate/<to_project>', methods=['GET', 'POST'])
def donate_to_project(to_project):
    return donate_render('donate.html', int(to_project))


def select_working_projects():
    projects = []
    lang = request.args.get('lang', 'ru')
    for p in app.models.Project.query.filter(app.models.Project.date_end.is_(None)).order_by("id").all():
        projects.append((p.id, p.name if lang == 'ru' else p.name_en))
    return projects

def split_by_prioritet(projects_radio_boxes):
    """Тут мы фильтруем проекты которые должны быть приоритетными"""
    priority_projects = [6, ]
    pane1_items, pane2_items = [], []
    for p in projects_radio_boxes:
        if p.data in priority_projects:
            pane1_items.append(p.id)
        else:
            pane2_items.append(p.id)
    return pane1_items, pane2_items

def split_items_by_prioritet(items):
    """Тут мы фильтруем проекты которые должны быть приоритетными"""
    priority_projects = [6, ]
    prioritet_items, not_prioritet= [], []
    for item in items:
        item_id, name = item
        if item_id in priority_projects:
            prioritet_items.append(item)
        else:
            not_prioritet.append(item)
    return prioritet_items, not_prioritet

def create_order_add_form(projects):
    order_form = OrderAddForm()
    order_form.project.choices = projects
    if projects and not order_form.project.data:
        # Установим значение по-умолчанию для контрола в первый элемент
        order_form.project.data = projects[0][0]
    return order_form

def donate_render(template_name, to_project=None, **kvargs):
    lang = request.args.get('lang', 'ru')
    items = select_working_projects()
    add_payment_form = create_order_add_form(items)
    url = url_for(u'donate')
    if to_project and not add_payment_form.project.data:
        add_payment_form.project.data = to_project
        url = url_for(u'donate_to_project', to_project=to_project)

    priority_pane_items, non_priority_pane_items = split_items_by_prioritet(items)
    p_pane_form = create_order_add_form(priority_pane_items)
    np_pane_form = create_order_add_form(non_priority_pane_items)

    # if add_payment_form.validate_on_submit():
    if request.method == 'POST':
        order = create_order(add_payment_form.project.data, add_payment_form.summa.data)
        create_sub_if_it_is_sub_order(add_payment_form.subs.data, order)
        response = submit_pay_redirect_form(order)
        return response
    else:
        session["lang"] = lang
        if add_payment_form.errors:
            print(u"add_payment_form.errors=", add_payment_form.errors)

    return render_template(
        template_name,
        caption=u'Пожертвовать:',
        url=url,
        lang=lang,
        show_choices=to_project,
        pane1_items=p_pane_form,
        pane2_items=np_pane_form,
        **kvargs)


def create_order(project_id, summa):
    order = models.Order()
    order.project_id = project_id
    order.summa = summa
    if current_user.is_authenticated:
        order.user_id = current_user.id
        order.email = current_user.email
    db.session.add(order)
    db.session.commit()
    return order


def create_sub_if_it_is_sub_order(sub_pay_choise, order):
    is_sub = sub_pay_choise > 1
    if is_sub :
        create_sub(order, months_to_end={1: 0, 2: 3, 3: 6, 4: 12, 5: 36}[sub_pay_choise])


def create_sub(order, months_to_end):
    sub = models.Sub()
    sub.date_beg = datetime.datetime.utcnow()
    sub.months_to_end = months_to_end
    sub.first_order_id = order.id
    db.session.add(sub)
    db.session.commit()
    order.sub_id = sub.id
    db.session.commit()


def submit_pay_redirect_form(order):
    response = flask_app.make_response(u"""
        <html>
            <body onload='document.forms[0].submit()'>
                Перенаправление на страницу оплаты...
                <form action='%s' method='post'>
                    <input type='hidden' name='Shop_IDP'      value='%s'>
                    <input type='hidden' name='Order_IDP'     value='%s'>
                    <input type='hidden' name='Subtotal_P'    value='%.2f'>
                    <input type='hidden' name='Signature'     value='%s'>
                    <input type='hidden' name='URL_RETURN_OK' value='%s'>
                    <input type='hidden' name='URL_RETURN_NO' value='%s'>
                    <input type='hidden' name='Email' 		  value='%s'>
                    <input type="hidden" name="Language"	  value='%s'>
                </form>
            </body>
        </html>
        """ % (flask_app.config["PAY_URL"],
               flask_app.config["SHOP_ID"],
               "%d" % order.id,
               order.summa,
               sign_order(order),
               flask_app.config["OK_URL"],
               flask_app.config["NO_URL"],
               order.email,
               session.get('lang', 'ru')))
    return response


def sign_order(order):
    sign = md5str(
        md5str(flask_app.config["SHOP_ID"]) + "&" +
        md5str("%d" % order.id) + "&" +  # Order ID
        md5str("%.2f" % order.summa) + "&" +  # Subtotal
        md5str("") + "&" +  # MeanType
        md5str("") + "&" +  # EMoneyType
        md5str("") + "&" +  # lifetime
        md5str("") + "&" +  # Customer_IDP
        md5str("") + "&" +  # Card_IDP
        md5str("") + "&" +  # IData
        md5str("") + "&" +  # PT_Code
        md5str(flask_app.config["SHOP_PASS"])
    ).upper()
    return sign
