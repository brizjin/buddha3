﻿# -*- coding: utf-8 -*-
# import datetime
from app import flask_app, db
from flask import render_template, session, request, flash, redirect, url_for
from flask_login import login_user, logout_user, current_user, login_required
from sqlalchemy.sql import text
# import app.forms  as forms
import app.models as models
import requests
from xml.etree import ElementTree


@flask_app.route("/orders")
@login_required
def orders():
    limit = request.args.get('limit', 100, type=int)
    page = request.args.get('page', 1, type=int)

    order_count = [r.count for r in db.engine.execute('select count(1) count from "order"')][0]
    pages = order_count // limit + (0 if (order_count % limit) == 0 else 1)

    if not page or page < 0 or page > pages:
        page = page if page > 0 else 1
        page = page if page < pages else pages
        return redirect(request.referrer or url_for('orders', page=page, limit=limit))

    orders = db.engine.execute(text('''
        select o.id
            ,o.date_beg
            ,o.summa
            ,coalesce(to_char(o.sub_id,'9999999'),'') sub_id
            ,o.status
            ,u.username
            ,p.name project_name
            --,row_number() over (partition by to_char(o.date_beg,'dd.mm.yyyy') order by o.id desc) first_row
            --,row_number() over (partition by to_char(o.date_beg,'dd.mm.yyyy') order by o.id) 	    last_row
        from "order" o
        left join "user" 	u on o.user_id 	  = u.id
        left join "project" p on o.project_id = p.id
        order by o.id desc
        LIMIT :limit OFFSET :offset
        '''), limit=limit, offset=(page - 1) * limit)  # .fetchall()

    return render_template(
        'orders.html'
        , active_nav=3
        , orders=orders
        , pages=pages
        , page=page
        , limit=limit
    )


@flask_app.route("/orders2")
@login_required
def orders2():
    limit = request.args.get('limit', 100, type=int)
    page = request.args.get('page', 1, type=int)

    order_count = [r.count for r in db.engine.execute('select count(1) count from "order"')][0]
    pages = order_count // limit + (0 if (order_count % limit) == 0 else 1)

    if not page or page < 0 or page > pages:
        page = page if page > 0 else 1
        page = page if page < pages else pages
        return redirect(request.referrer or url_for('orders', page=page, limit=limit))

    table = db.engine.execute(text(u'''
        select o.id
            ,o.date_beg		Дата
            ,o.summa		Сумма
            --,coalesce(to_char(o.sub_id,'9999999'),'') sub_id
            ,coalesce(o.status,' ') Статус
            ,coalesce(recommendation,' ') Подсказка
            ,coalesce(message,' ') Сообщение
            ,coalesce(email,' ') Email
            ,coalesce(cardholder,' ') ФИО
            --,u.username
            --,p.name project_name
            --,row_number() over (partition by to_char(o.date_beg,'dd.mm.yyyy') order by o.id desc) first_row
            --,row_number() over (partition by to_char(o.date_beg,'dd.mm.yyyy') order by o.id) 	    last_row
        from "order" o
        --left join "user" 	u on o.user_id 	  = u.id
        --left join "project" p on o.project_id = p.id
        order by o.id desc
        LIMIT :limit OFFSET :offset
        '''), limit=limit, offset=(page - 1) * limit)  # .fetchall()

    f = {2: '{:%d.%m.%Y %H:%M}', 3: '{0:,.2f}'}

    return render_template('table.html', active_nav=3, table=table, formats=f)


XML = 1
CSV = 2


# @celery_app.task
def get_status_from_order_detail_request(order_id, text_format=XML):
    print("Запрос статуса для заказа {0}".format(order_id))
    order = db.session.query(models.Order).get(order_id)
    if order is None:
        return
    request_text = requests.get(flask_app.config["RESULTS_URL"],
                                params={
                                    'Shop_ID': flask_app.config["SHOP_ID"],
                                    'Login': flask_app.config["LOGIN"],
                                    'Password': flask_app.config["SHOP_PASS"],
                                    'ShopOrderNumber': "%s" % order_id,
                                    'Format': 4}).content  # .decode('utf-8')#для питона 3
    a = {}
    if text_format == XML:
        r = ElementTree.fromstring(request_text)
        r = r.findall('.//orders/order/')
        for child in r:
            a[child.tag.lower()] = child.text
    # elif text_format == CSV:
    #     arr = request_text.split("\n")
    #     a = dict(zip([a.lower() for a in arr[0].split(";")], arr[1].split(";")))

    update_order_by_dict(order, a)

    # order_update_id = a.get("ordernumber", None)
    # if order_update_id:
    #     order = db.session.query(models.Order).get(order_update_id)  # удалить 1: для боевой
    #     old_status = order.status
    #     # order.status = a.get("recommendation",None)
    #     order.status = a.get("status", None)
    #     order.recommendation = a.get("recommendation", None)
    #     order.message = a.get("message", None)
    #     order.email = a.get("email", None)
    #     order.cardholder = a.get("cardholder", None)
    #     if order.status:
    #         order.status = order.status.lower()
    #     # if status:
    #     # 	order.status = status
    #     # else:
    #     # 	order.status = u'Статус неизвестен'
    #     if old_status != order.status:
    #         print('Статус заказа {0} был изменен с {1} на {2}'.format(order_id, old_status, order.status))
    # if not order.status_up:
    #     order.status_up = 0
    # order.status_up += 1
    # db.session.commit()

    return a


def update_order_by_dict(order, a):
    old_status = order.status
    order.status = a.get("status", None)
    order.recommendation = a.get("recommendation", None)
    order.message = a.get("message", None)
    order.email = a.get("email", None)
    order.cardholder = a.get("cardholder", None)
    if order.status:
        order.status = order.status.lower()
    if old_status != order.status:
        print('Статус заказа {0} был изменен с {1} на {2}'.format(order.id, old_status, order.status))
    if not order.status_up:
        order.status_up = 0
    order.status_up += 1
    db.session.commit()


@flask_app.route('/order_detail/<order_id>')
def order_detail(order_id):
    a = get_status_from_order_detail_request(order_id)
    return render_template('order_detail.html', active_nav=4, u=a)
