# -*- coding: utf-8 -*-
import datetime

from app import flask_app, db
from flask import render_template, request, url_for, redirect
from flask_login import current_user
from app.forms import *
import app.models as models


@flask_app.route("/projects")
def projects_view():
    return render_template('projects.html', active_nav=4, projects=models.Project.query.order_by("id").all())


@flask_app.route('/projects/add', methods=['GET', 'POST'])
def project_add():
    pform = ProjectForm()
    if pform.validate_on_submit():
        # project = models.Project(pform.name.data, pform.caption.data)
        project = models.Project()
        project.name = pform.name.data
        project.name_en = pform.name_en.data
        db.session.add(project)
        db.session.commit()
        return render_template('add_success.html', name=pform.name.data)
    else:
        return render_template('add_form.html', title='Форма оплаты', caption=u'Добавление нового проекта', pform=pform)


@flask_app.route('/projects/<action>/<project_id>', methods=['GET', 'POST'])
def project_close(action, project_id):
    prj = db.session.query(models.Project).get(project_id)
    if action == 'close':
        prj.date_end = datetime.datetime.now()
    elif action == 'continue':
        prj.date_end = None
    db.session.commit()
    return redirect(request.args.get('next') or request.referrer or url_for('projects'))
