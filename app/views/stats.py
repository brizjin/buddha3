# -*- coding: utf-8 -*-
import datetime
from app import flask_app,db
from flask import render_template, session, request,flash,redirect,url_for
from flask_login import login_user, logout_user, current_user, login_required
from sqlalchemy.sql import text
import app.forms  as forms
import app.models as models
import sys

#from flask.ext.wtf import Form
from flask_wtf import FlaskForm
from wtforms import TextField, BooleanField, SelectField, DecimalField, RadioField, PasswordField, DateField, DateTimeField, TextAreaField, SubmitField, StringField
from wtforms.validators import Required, Optional
import app.models
from wtforms.widgets.core import SubmitInput

@flask_app.route("/stats")
@login_required
def stats():
    table = db.engine.execute(text(u'''
         select p.id "Номер проекта"
        ,p.name Проект
        ,coalesce(sum(o.summa),0) "Общая сумма сборов"
        --,'Привет' "Новая колонка"--,o.date_beg
                                        from "project" p
                                        left join "order" o on o.project_id = p.id and status = 'paid'
                                        group by p.id,p.name
                                        order by p.id'''))

    f = {3: '{0:,.2f}'}

    return render_template('table.html', active_nav = 6, table = table, formats = f)

@flask_app.route("/stats_months")
@login_required
def stats_m():
    table = db.engine.execute(text(u'''
         select p.id "Номер проекта"
            ,coalesce(date_trunc('month',o.date_beg),'2015-01-01') Месяц
            ,p.name Проект
            ,coalesce(sum(o.summa),0) "Общая сумма сборов"

            --,'Привет' "Новая колонка"--,o.date_beg
        from "project" p
        left join "order" o on o.project_id = p.id and status = 'paid'
        group by p.id,p.name,date_trunc('month',o.date_beg)
        order by coalesce(date_trunc('month',o.date_beg),'2015-01-01') desc'''))

    f = {2:'{:%m.%Y}',4:'{0:,.2f}'}

    return render_template('table.html', active_nav = 6, table = table, formats = f)




@flask_app.route("/sm", methods = ['GET', 'POST'])
@login_required
def stats_sm():
    sql = '''
         select coalesce(date_trunc('day',o.date_beg at time zone 'Europe/Moscow'),'2015-01-01') Месяц
            ,p.name Проект
            ,coalesce(sum(o.summa),0) "Общая сумма сборов"

            --,'Привет' "Новая колонка"--,o.date_beg
        from "project" p
        left join "order" o on o.project_id = p.id and status = 'paid'
        where p.id = :prj or :prj is null
        group by p.id,p.name,date_trunc('day',o.date_beg at time zone 'Europe/Moscow')
        order by coalesce(date_trunc('day',o.date_beg at time zone 'Europe/Moscow'),'2015-01-01') desc'''

    f = {1:'{:%d.%m.%Y}',3:'{0:,.2f}'}
    
    class MySubmitInput(SubmitInput):

        def __call__(self, field, **kwargs):
            kwargs['class'] = u'%s %s' % (kwargs.get('class',''), 'form-control btn btn-success')
            return super(MySubmitInput, self).__call__(field, **kwargs)

    class Test(FlaskForm):
        #text    	= TextField     (u"Проект", validators = [Required("Необходимо заполнить фильтр каким-либо значением")], description = { "autofocus" : True})
        prj    	    = SelectField   (u"Проект", coerce=int, choices=[(1,u'Без подписки'),(2,u'Три месяца'),(3,u'Пол года'),(4,u'Год'),(5,u'Три года')], default = 1)
        date_beg    = DateTimeField (u"Дата начала"   ,format='%d.%m.%Y')
        date_end    = DateTimeField (u"Дата окончания",format='%d.%m.%Y')
        #submit 	    = SubmitField   (u"Показать")
        
        submit 	    = SubmitField   (u"Показать")
        submit2      = SubmitField   (u"TEST",widget = MySubmitInput())
        #submit.kwargs = {"class":"form-control btn btn-success"}

        url         = '/sm'


    form = Test()
    form.prj.choices = [(row["id"],row["name"]) for row in db.engine.execute(text(u'''select id,name from project'''))]

    if form.is_submitted():
        form.date_beg.process_errors.remove(u'Not a valid datetime value')
        form.date_end.process_errors.remove(u'Not a valid datetime value')

    if form.validate_on_submit():
        table = db.engine.execute(text(sql),prj = form.prj.data)
        return render_template('table.html', active_nav = 6, table = table, formats = f, form = form)
    else:
        table = db.engine.execute(text(sql),prj = '1')
        
        return render_template('table.html', active_nav = 6, table = table, formats = f, form = form)