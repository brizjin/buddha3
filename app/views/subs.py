# -*- coding: utf-8 -*-
from __future__ import print_function

import datetime
import hashlib
import logging
import os
from collections import namedtuple
from threading import Thread

import requests
from flask import render_template, request, flash, redirect, url_for
from flask_login import current_user, login_required
from flask_wtf import FlaskForm
from sqlalchemy.sql import text

import app.models as models
from app import flask_app, db
from app.lib import md5 as md5str
from app.views.orders import get_status_from_order_detail_request, CSV

logger = logging.getLogger(__name__)


def read_sql(file_name):
    return open(os.path.join(flask_app.config['APP_PATH'], 'sql', file_name), 'rb').read().split('--where_to_split')[0]


def read_sql2(file_name):
    with open(os.path.join(flask_app.config['APP_PATH'], 'sql', file_name), 'r+') as f:
        return f.read().decode("utf-8-sig").encode("utf-8")


# sub_info = read_sql('sub_info.sql')
sub_info = read_sql2('sub_info.sql')
# sub_plan = read_sql('sub_plan.sql')
sub_plan = read_sql2('sub_plan.sql') % sub_info


def execute(sql, hide=False, check=False, actions=None):
    class TableForm(FlaskForm):
        pass

    class Column:
        def __init__(self, name):
            self.name = name
            self.format_code = ''
            self.alias = ''
            self.hide = True

        def get_alias(self):
            return self.alias if self.alias else self.name

    class Rows(list):
        def __init__(self, rows, keys):
            super(Rows, self).__init__()
            self.extend(rows)
            self.keys = keys
            self.columns = dict((k, Column(k)) for k in keys)
            # self.aliases = aliases
            # self.aliases = dict((k, Column(k)) for k in columns)
            # self.formatters = dict()
            self.hide = hide
            self.check = check
            self.actions = actions
            if check:
                self.form = TableForm()

            self.hashs = {}
            self.total = []
            for i, r in enumerate(self):
                self.hashs[self.get_hash(i)] = r

        def set_column_alias(self, column_name, alias=None, format_code=None):
            column = self.columns.get(column_name)
            if column:
                column.hide = False
                if alias:
                    column.alias = alias
                if format_code:
                    column.format_code = format_code

        # def get_columns(self):
        #     return [column for column in self.columns]

        def get_rows(self):
            def format_column(value, name):
                column = self.columns.get(name)
                if column and column.format_code == 'month':
                    if value:
                        month = {1: u'Январь', 2: u'Февраль', 3: u'Март', 4: u'Апрель', 5: u'Май', 6: u'Июнь',
                                 7: u'Июль', 8: u'Август',
                                 9: u'Сентябрь', 10: u'Октябрь', 11: u'Ноябрь', 12: u'Декабрь', }[value.month]
                        return '%s %s' % (month, value.year)
                elif isinstance(value, datetime.datetime):
                    if value:
                        month = {1: u'Января', 2: u'Февраля', 3: u'Марта', 4: u'Апреля', 5: u'Мая', 6: u'Июня',
                                 7: u'Июля', 8: u'Августа', 9: u'Сентября', 10: u'Октября', 11: u'Ноября',
                                 12: u'Декабря', }[value.month]
                        return ('%02d %s %s' % (value.day, month, value.year)).lower()
                else:
                    return value

            for row in self:
                ra = [format_column(v, self.keys[i]) for i, v in enumerate(row)]
                yield ra

        def get_hash(self, index):
            return hashlib.md5(str(self[index])).hexdigest()

        def get_checked(self, name):
            hash_list = request.form.getlist(name)
            row_list = []
            for h in hash_list:
                row_list.append(self.hashs[h])
            return row_list

        def add_total(self, column_name):
            self.total.append(column_name)

        def get_total(self, column_name):
            if column_name in self.total:
                i = self.keys.index(column_name)
                s = 0
                for row in self:
                    s += row[i]
                return s

    exe = db.engine.execute(sql)
    return Rows(exe.fetchall(), exe.keys())


@flask_app.route("/subs", methods=['GET', 'POST'])
@login_required
def subs():
    subs_list = execute("""
        select row_number() OVER (ORDER BY a.date_beg)	n, a.*
        from (%s) a
        where sum_payed < sum_total order by date_beg desc""" % sub_info, hide=True)
    subs_list.set_column_alias("n", "Номер")
    subs_list.set_column_alias("sub_id", "Подписка")
    subs_list.set_column_alias("date_beg", "Дата начала")
    # subs_list.set_column_alias("date_end", "Дата окончания")
    subs_list.set_column_alias("months_to_end", "Кол-во")
    subs_list.set_column_alias("summa", "Сумма")
    subs_list.set_column_alias("sum_payed", "Факт")
    subs_list.set_column_alias("sum_total", "Всего")
    # subs_list.set_column_alias("status", "Статус")
    subs_list.set_column_alias("email", "Почта")
    subs_list.set_column_alias("phone", "Телефон")
    subs_list.add_total("summa")

    return render_template('subs.html', rows=subs_list)


@flask_app.route("/pay_subs", methods=['GET', 'POST'])
@login_required
def pay_subs():
    def sub():
        subs_list = execute("select a.* from (%s) a where date_paid is null and status = 'active'" % sub_plan,
                            check=True, actions={"pay": "Оплатить"}, hide=False)
        # subs_list.set_column_alias("n", "N")
        subs_list.set_column_alias("date_plan", "Плановая дата")
        subs_list.set_column_alias("summa", "Сумма")
        # subs_list.set_column_alias("project_name", "Проект")
        subs_list.set_column_alias("sub_id", "Номер подписки")
        subs_list.set_column_alias("sum_payed", "Факт")
        subs_list.set_column_alias("sum_plan", "План")
        subs_list.set_column_alias("date_beg", "Дата подписки")
        subs_list.set_column_alias("months_to_end", "Месяцев")
        subs_list.set_column_alias("email", "Почта")
        subs_list.set_column_alias("phone", "Телефон")
        # subs_list.set_column_alias("date_paid", "Дата оплаты")
        return subs_list

    subs_list = sub()
    if request.method == 'POST' and request.form['action'] == 'pay':

        Recurrent = namedtuple('RecurerntPays',
                               ['project_id', "summa", "sub_id", "user_id", "date_plan", "first_order_id"])
        rs = []
        for r in subs_list.get_checked('ids'):
            rs.append(Recurrent(r.project_id, r.summa, r.sub_id, r.user_id, r.date_plan, r.first_order_id))

        def pay(recurrent_pays_info):
            for pay_info in recurrent_pays_info:
                try:
                    logger.debug('LOG')
                    recurrent_pay(pay_info.project_id, pay_info.summa, pay_info.sub_id, pay_info.user_id,
                                  pay_info.date_plan, pay_info.first_order_id)
                except Exception:
                    logger.exception("recurrent_pay")
            # subs_list = sub()

        t = Thread(target=pay, args=(rs,))
        t.daemon = True
        t.start()
    return render_template('subs.html', rows=subs_list)


def recurrent_pay(project_id, summa, sub_id, user_id, pay_for_date, first_order_id):
    try:
        order = models.Order()
        order.project_id = project_id
        order.summa = summa
        order.sub_id = sub_id
        order.user_id = user_id
        order.date_for = pay_for_date
        order.status = u'created'
        db.session.add(order)
        db.session.commit()

        # flash(u'Успешно произведенна оплата по подписке №%s'%order.sub_id,'success')
        lsign = md5str(md5str(flask_app.config["SHOP_ID"]) + "&" +
                       md5str("%d" % order.id) + "&" +
                       md5str("%.2f" % order.summa) + "&" +
                       md5str("%d" % first_order_id) + "&" +
                       md5str(flask_app.config["SHOP_PASS"])).upper()
        data = {'Shop_IDP': flask_app.config["SHOP_ID"],
                'Order_IDP': "%d" % order.id,
                'Subtotal_P': "%.2f" % order.summa,
                'Parent_Order_IDP': "%s" % first_order_id,
                'Signature': lsign}
        # if not flask_app.debug:
        r = requests.post(flask_app.config["RECURENT_URL"], data=data)
        # get_status_from_order_detail_request(r.content, CSV)
        # get_status_from_order_detail_request(order.id)
        try:
            logger.debug("response=%s" % r.content)
        except Exception:
            pass
        header, first_data_line = r.content.partition("\n")[::2]
        a = dict(zip([a.lower() for a in header.split(";")], first_data_line.split(";")))
        # update_order_by_dict(order, a)
        order.status = a.get("status", None).lower()
        db.session.commit()
    except Exception as e:
        logger.exception("recurrent_pay")


@flask_app.route("/my_subs")
@login_required
def my_subs():
    subs_list = db.engine.execute(
        text('''select * from (%s) a WHERE user_id = :user_id order by date_beg desc''' % sub_info),
        user_id=current_user.id).fetchall()
    subs_plan = db.engine.execute(text('''select * from (%s) a WHERE user_id = :user_id''' % sub_plan),
                                  user_id=current_user.id).fetchall()
    return render_template('my_subs.html', subs=subs_list, subs_plan=subs_plan)


@flask_app.route('/unsub')
@login_required
def unsub():
    sub_id = request.args.get('sub_id')
    sql_text = text('''select * from (%s) a WHERE sub_id = :sub_id ''' % sub_info)
    sub_row = db.engine.execute(sql_text, sub_id=sub_id).fetchone()
    if not sub_row:
        raise Exception(u"Подписка №%s не найдена" % sub_id)
    if current_user.id != sub_row.user_id and not current_user.is_admin:
        raise Exception(u"Отписаться можно только от своей подписки")
    if sub_row.status == 'close':
        raise Exception(u"Подписка уже не действует")
    if sub_row.status == 'not started':
        raise Exception(u"Подписка даже не была активированна")
    db.engine.execute(text('''UPDATE sub SET date_end = now() WHERE id = :sub_id'''), sub_id=sub_id)
    db.session.commit()
    flash(u"Вы успешно отписанны от подписки №%s" % sub_id, "success")
    return redirect(request.args.get('next') or request.referrer or url_for('my_subs'))


@flask_app.route('/continue_sub')
@login_required
def continue_sub():
    sub_id = request.args.get('sub_id')
    sub_row = db.engine.execute(text('''select * from (%s) a WHERE a.sub_id = :sub_id''' % sub_info),
                                sub_id=sub_id).fetchone()
    if not sub_row:
        raise Exception(u"Подписка №%s не найдена" % sub_id)
    if current_user.id != sub_row.user_id and not current_user.is_admin:
        raise Exception(u"Продолжить можно только свою подписку")
    if sub_row.status == 'active':
        raise Exception(u"Подписка уже действует")
    if sub_row.status == 'not started':
        raise Exception(u"Подписка даже не была активированна")
    db.engine.execute(text('''UPDATE sub SET date_end = null WHERE id = :sub_id'''), sub_id=sub_id)
    db.session.commit()
    flash(u"Вы успешно продолжили подписку №%s" % sub_id, "success")
    return redirect(request.args.get('next') or request.referrer or url_for('my_subs'))


# @flask_app.route("/subs_period", methods=['GET', 'POST'])
# @login_required
# def subs_period():
#     form = forms.DatePeriodFilterForm()
#     if form.validate_on_submit() or session.get('subs_period_date_beg') and session.get('subs_period_date_end'):
#         if session.get('subs_period_date_beg')
#         and session.get('subs_period_date_end') and not form.validate_on_submit():
#             form.date_beg.data = date_beg = session['subs_period_date_beg']
#             form.date_end.data = date_end = session['subs_period_date_end']
#         else:
#             session['subs_period_date_beg'] =
#             date_beg = form.date_beg.data.replace(hour=0, minute=0, second=0, microsecond=0)
#             session['subs_period_date_end'] =
#             date_end = form.date_end.data.replace(hour=0, minute=0, second=0, microsecond=0)
#
#         if current_user.role == 1:
#             if form.validate_on_submit() and request.form['action'] == 'do':
#                 subs_pay_for_period(date_beg, date_end)
#
#         else:
#             flash(u'Необходимы права администратора','danger')
#             redirect(request.args.get('next') or url_for('subs_period'))
#
#     else:
#         form.date_beg.data = form.date_end.data = date_beg = date_end = datetime.datetime.utcnow()
#
#     subs_list = db.engine.execute(text('select * from subs_to_pay(:date_beg,:date_end)'),
#                                   date_beg=date_beg,
#                                   date_end=date_end)
#     return render_template('subs.html', active_nav=7, subs=subs_list, form=form)


# @celery_app.task
def sub_pay(sub_id, date_for):
    try:
        sub = db.engine.execute(
            text('''select * from subs_to_pay(:date_for,:date_for) s where not is_paid and s.sub_id = :sub_id'''),
            date_for=date_for,
            sub_id=sub_id)
        if sub.rowcount > 1:
            print("Ошибка. Найденно больше одной подписки для оплаты.")
            return sub.rowcount
        if sub.rowcount == 0:
            print("Ошибка. Не найденно подписок для оплаты.")
            return sub.rowcount
        if sub.rowcount == 1:
            for s in sub:
                # sub_first_order  	= db.session.query(models.Order).get(s.first_order)
                # sub_last_paid_order = s.last_paid_order
                order = models.Order()
                order.project_id = s.project_id
                order.summa = s.first_order_summa
                order.sub_id = s.sub_id
                order.user_id = s.user_id
                order.date_for = s.date_to_pay
                order.status = u'created'
                db.session.add(order)
                db.session.commit()

                # flash(u'Успешно произведенна оплата по подписке №%s'%order.sub_id,'success')
                lsign = md5str(md5str(flask_app.config["SHOP_ID"]) + "&" +
                               md5str("%d" % order.id) + "&" +
                               md5str("%.2f" % order.summa) + "&" +
                               md5str("%d" % s.last_paid_order) + "&" +
                               md5str(flask_app.config["SHOP_PASS"])).upper()
                data = {'Shop_IDP': flask_app.config["SHOP_ID"],
                        'Order_IDP': "%d" % order.id,
                        'Subtotal_P': "%.2f" % order.summa,
                        'Parent_Order_IDP': "%s" % s.last_paid_order,
                        'Signature': lsign}
                # if not flask_app.debug:
                r = requests.post(flask_app.config["RECURENT_URL"], data=data)
                get_status_from_order_detail_request(r.content, CSV)
                # отправим уведомление о списании денег
                # try:

                # except Exception, e:
                # print u'Error of sending mail',traceback.format_exc(),e

    except Exception as e:
        import traceback
        print(u'Error of pay:', str(e))
        traceback.print_exc()

    return True


@flask_app.route('/sub_pay_view', methods=['GET'])
@login_required
def sub_pay_view():
    sub_id = request.args.get('sub_id')
    date_for = request.args.get('date_for')

    if not sub_id:
        flash(u'ID подписки небыл передан', 'danger')
        return redirect(request.args.get('next') or url_for('subs_period'))
    if not date_for:
        flash(u'Дата не переданна', 'danger')
        return redirect(request.args.get('next') or url_for('subs_period'))

    sub = db.session.query(models.Sub).get(sub_id)
    if not sub:
        flash(u'Не удается найти подписку №%s' % sub_id, 'danger')
        return redirect(request.args.get('next') or url_for('subs_period'))

    try:
        d_date_for = datetime.datetime.strptime(date_for, '%d%m%Y')
    except ValueError:
        flash(u"Не удалось создать дату по строке %s с форматом ddmmyyyy" % date_for, 'danger')
        return redirect(request.args.get('next') or url_for('subs_period'))

    flash(u"Платеж по подписке №{sub_id} за дату {date_for:%d%m%Y} успешно отправлен на оплату"
          .format(sub_id=sub_id, date_for=d_date_for), 'success')
    # sub_pay.apply_async(args=[sub_id, d_date_for], countdown=0)
    sub_pay(sub_id, d_date_for)
    return redirect(request.args.get('next') or request.referrer or url_for('subs_period'))
