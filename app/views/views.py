# -*- coding: utf-8 -*-
import hashlib
import os

import datetime
import requests
from flask import render_template, jsonify, request, send_from_directory, flash, url_for, redirect, g
from flask_login import current_user, login_required
from sqlalchemy import desc

import app.forms as forms
from app import celery_app
from app import flask_app, db, lm
from app import mail
from app.forms import *
from app.models import Order, ROLE_ADMIN, Project, User
from app.mymail import Message
from app.views.donate import donate_render, md5str


# @flask_app.before_request
# def before_request():
#     if request.url.startswith('http://') and not request.headers['Host'].startswith('localhost'):
#         url = request.url.replace('http://', 'https://', 1)
#         code = 301
#         return redirect(url, code=code)

@flask_app.route("/")
def index():
    return donate_render('layout.html', active_nav=1)


@flask_app.route('/fonts/<path:filename>')
def send_foo(filename):
    return send_from_directory(os.path.join(flask_app.config["BASEDIR"], 'app', 'static', 'fonts'), filename)


@flask_app.route("/my_orders")
@login_required
def my_orders():
    orders = []
    if current_user.is_authenticated():
        orders = Order.query \
            .filter(Order.user_id == current_user.id, Order.status.in_(['paid', 'authorized'])) \
            .order_by(desc(Order.id)).all()
    return render_template('my_orders.html', active_nav=5, orders=orders)


@flask_app.route("/sign")
def sign():
    summa = request.args.get('summa', 0, type=int)
    project_id = request.args.get('project_id', 1, type=int)

    order = Order()
    order.summa = summa
    order.project_id = project_id
    db.session.add(order)
    db.session.commit()

    is_test = False
    if is_test:
        # url = "https://test.wpay.uniteller.ru/pay/"
        shop_id = "400075880000000-1258"
    else:
        # url = "https://wpay.uniteller.ru/pay/"
        shop_id = "00001933"

    order_id = order.id
    # print "ORDER_ID=",order_id
    subtotal = order.summa
    # lifetime = 600
    password = "uz9SjIuhrlrAump25Bjx5UDeUf5ZcUXxT9tbEEQtnzHQaajDCyR8gTWmPcNIiyXnE0zvmOUInn5anWp6"  # NadezhdaT-S/82feYk

    def fmd5str(value):
        return hashlib.md5.new(value).hexdigest()

    # m = hashlib.md5()
    # m.update(value)
    # return m.hexdigest()

    lsign = fmd5str(
        md5str(shop_id) + "&" +
        md5str("%d" % order_id) + "&" +  # Order ID
        md5str("%.f" % subtotal) + "&" +  # Subtotal
        md5str("") + "&" +  # MeanType
        md5str("") + "&" +  # EMoneyType
        md5str("") + "&" +  # lifetime
        md5str("") + "&" +  # Customer_IDP
        md5str("") + "&" +  # Card_IDP
        md5str("") + "&" +  # IData
        md5str("") + "&" +  # PT_Code
        md5str(password)
    ).upper()

    return jsonify(sign=lsign, order=order_id, summa=subtotal)


@celery_app.task
def send_notification_mail(order_id):
    with flask_app.app_context():
        order = db.session.query(Order).get(order_id)
        mailaddr = order.user.email if not flask_app.debug else "brizjin@gmail.com"
        msg = Message(u'Уведомление о подписке', sender='hello@gmail.com', recipients=[mailaddr])
        verify = md5str(str(order.user_id) + flask_app.config["SHOP_PASS"])
        # flask_app.config.update(SERVER_NAME = 'donate.buddhism.ru',)
        # unsubscribe_url = url_for(".email_notify",sub=0,verify=verify,user_id=order.user_id)
        unsubscribe_url = "{domain_name}/email_notify?is_notify={is_notify}&verify={verify}&user_id={user_id}" \
            .format(domain_name=flask_app.config["DOMEN_STRING"], is_notify=0, verify=verify, user_id=order.user_id)
        # flask_app.config.update(SERVER_NAME = None)
        # print('unsub',unsubscribe_url)
        msg.html = render_template('mail_sub_pay.html', order=order, unsubscribe_url=unsubscribe_url)
        mail.send(msg)
        print(u'Письмо успешно отправленно')


@flask_app.route('/untlr/update_status', methods=['GET', 'POST'])
def update_status():
    # log = models.Log()
    order_id = request.form.get('Order_ID', 'Nonee')
    status = request.form.get('Status', 'Nonee')
    lsign = request.form.get('Signature', 'Nonee')

    # print("STATUS",order_id,status,sign,md5str(order_id + status + flask_app.config["SHOP_PASS"]).upper())

    if lsign == md5str(order_id + status + flask_app.config["SHOP_PASS"]).upper():
        order = db.session.query(Order).get(order_id)
        order.status = status.lower()
        db.session.commit()
        if order.user and order.user.is_notify and status.upper() == 'PAID':
            send_notification_mail.s(order_id).apply_async()
    return 'ok'


@flask_app.route('/send_update')
def send_update_status():
    if flask_app.debug:
        lid = request.args['id']
        status = request.args['status']
        # print("ID",id)
        data = {'Order_ID': lid,
                'Status': status,
                'Signature': md5str(lid + status + flask_app.config["SHOP_PASS"]).upper()}
        url = flask_app.config["DOMEN_ADR"] + url_for('update_status')
        r = requests.post(url, data=data)
        return r.content
    return 'Только в режиме отладки возможно посылать запрос'


add_payment_cache = dict()


@flask_app.route('/orders/add/<project_id>', methods=['GET', 'POST'])
def add_payment(project_id):
    lang = request.args.get('lang', 'ru')
    if lang != 'ru':
        lang = 'en'

    add_payment_form = OrderAddForm()

    # if project_id != 1:
    #	project_id = 1
    # if project_id == 1:
    #	project_name    = u"Пожертвование на проект \"Биг Московский\""
    #	project_name_en = u"Join to project \"Big Moskovsky\""
    prj = db.session.query(Project).get(project_id)
    project_name = prj.name
    project_name_en = prj.name_en
    # if lang == 'test':
    #	project_name_en = request.__dict__

    url = "https://wpay.uniteller.ru/pay/"
    shop_id = "00001933"

    rt = ''

    if (lang, project_id) not in add_payment_cache:
        if lang != 'ru':
            rt = render_template('add_order_en.html', caption=u'Join to project',
                                 form=add_payment_form, shop_id=shop_id, url=url, project_name=project_name_en)
        else:
            rt = render_template('add_order.html', caption=u'Участвовать в проекте:',
                                 form=add_payment_form, shop_id=shop_id, url=url, project_name=project_name)

        add_payment_cache[(lang, project_id)] = rt
    else:
        rt = add_payment_cache[(lang, project_id)]

    return rt


@flask_app.route('/email_notify')
# @login_required
def email_notify():
    try:
        is_notify = request.args.get('is_notify')
        verify = request.args.get('verify')
        user_id = request.args.get('user_id')
        if user_id:
            u = db.session.query(User).get(user_id)
            if not u:
                flash(u"Пользователь не найден", u"danger")
                return redirect(request.args.get('next') or request.referrer or url_for(
                    'my_subs') if current_user.is_authenticated() else url_for('index'))
        else:
            u = current_user

        if not current_user.is_authenticated() and verify != md5str(str(user_id) + flask_app.config["SHOP_PASS"]):
            flash(u"Не верный проверочный код", u"danger")
            return redirect(url_for('index'))

        if is_notify in ('0', '1'):
            u.is_notify = is_notify
            db.session.commit()

            if u.is_notify:
                flash(u"Информирование на адрес %s успешно подключенно" % u.email, u"success")
            else:
                flash(u"На адрес %s вы больше не будете получать уведомления" % u.email, u"success")
        else:
            flash('Не возможно изменить статус подписки', 'danger')

    except Exception as e:
        flash(str(e), u"danger")

    return redirect(request.args.get('next') or request.referrer or url_for(
        'my_subs') if current_user.is_authenticated() else url_for('index'))


@flask_app.route("/fail")
def fail():
    return render_template('fail.html')


@flask_app.route("/ok")
def ok():
    return render_template('ok.html')


@flask_app.route("/offert")
def offert():
    return render_template('offert.html')


@flask_app.route("/create_all")
def create_all_view():
    db.create_all()
    return render_template('tables_create_all.html', active_nav=1)


@lm.user_loader
def load_user(userid):
    return db.session.query(User).get(userid)


@flask_app.before_request
def before_request():
    g.user = current_user


@flask_app.route('/send_invite', methods=['GET', 'POST'])
@login_required
def send_invite():
    if not current_user.is_admin:
        flash(u"Недостаточно прав. Для отправки приглашения нужно обладать правами администратора. " +
              u"Авторизируйтесь как администратор.", 'danger')
        return redirect(url_for('login', next=url_for('send_invite')))
    else:
        form = forms.SendInviteForm()
        if not form.validate_on_submit():
            return render_template('base_dialog_form.html', form=form)
        else:

            msg = Message('Приглашение на donate.buddhism.ru', sender='hello@gmail.com', recipients=[form.email.data])
            msg.html = render_template('mail_invite.html', mail=form.email.data,
                                       hash=hashlib.md5(form.email.data + form.email.data).hexdigest())
            mail.send(msg)

            flash(u"Приглашение успешно отправлено.", 'success')
            return redirect(url_for('send_invite'))


@flask_app.route('/users', methods=['GET', 'POST'])
@login_required
def users():
    if not current_user.is_admin:
        flash(u"Недостаточно прав. Для просмотра списка пользователей " +
              u"нужно обладать правами администратора. Авторизируйтесь как администратор.", 'danger')
        return redirect(url_for('login', next=url_for('send_invite')))
    else:

        return render_template('users_table.html',
                               active_nav=5, users=User.query.order_by("date_reg desc nulls last").all())


@flask_app.route('/invite_reg/<email>/<hashpass>', methods=['GET', 'POST'])
def invite_reg(email, hashpass):
    if hashlib.md5(email + email).hexdigest() != hashpass:
        return render_template('show_message.html', msg_header=u"Регистрация по приглашению",
                               msg_body=u"Проверочный код неверен.")
    else:
        form = forms.IniteRegForm()

        # form.email = None
        if not form.validate_on_submit():
            # if db.session.query(models.User).filter_by(email    = email).all():
            # 	flash(u'Пользователь с email %s уже существует'%email,u'danger')
            # 	return redirect(url_for('login'))
            return render_template('base_dialog_form.html', form=form)
        else:
            if db.session.query(User).filter_by(username=form.username.data).all():
                flash(u'Пользователь с именем %s уже существует' % form.username.data, u'danger')
                return render_template('base_dialog_form.html', form=form)
            if db.session.query(User).filter_by(email=email).all():
                flash(u'Пользователь с email %s уже существует' % email, u'danger')
                return render_template('base_dialog_form.html', form=form)

            user = User()
            user.username = form.username.data
            user.userpassword = hashlib.md5(form.username.data + form.password.data).hexdigest()  # form.password.data
            user.mailpassword = hashlib.md5(email + form.password.data).hexdigest()  # form.password.data
            user.email = email
            user.phone = form.phone.data
            db.session.add(user)
            db.session.commit()

            msg = Message('Регистрация на donate.buddhism.ru', sender='hello@gmail.com', recipients=[email])
            msg.html = u'''
                <h4>Вы успешно зарегестрированы на donate.buddhism.ru</h4>
                <table id="orders_table" class="table borderless" style="margin-bottom: 0px;">
                    <tr><td style="border: none;">Логин</td><td style="border: none;">{0}</td></tr>
                    <tr><td style="border: none;">Почта</td><td style="border: none;">{1}</td></tr>
                    <tr><td style="border: none;">Пароль</td><td style="border: none;">{2}</td></tr>
                </table>'''.format(form.username.data, email, form.password.data)

            mail.send(msg)

            flash(u'Пользователь %s успешно зарегистрован' % user.username, u'success')
            return redirect(url_for('login'))
        # return render_template('show_message.html',msg_header=u"Регистрация по приглашению",msg_body=u"OK")


@flask_app.route('/download')
def download():
    csv = """"REVIEW_DATE","AUTHOR","ISBN","DISCOUNTED_PRICE"
"1985/01/21","Douglas Adams",0345391802,5.95
"1990/01/12","Douglas Hofstadter",0465026567,9.95
"1998/07/15","Timothy ""The Parser"" Campbell",0968411304,18.99
"1999/12/03","Richard Friedman",0060630353,5.95
"2004/10/04","Randel Helms",0879755725,4.50"""
    # We need to modify the response, so the first thing we
    # need to do is create a response out of the CSV string
    response = flask_app.make_response(csv)
    # This is the key: Set the right header for the response
    # to be downloaded, instead of just printed on the browser
    response.headers["Content-Disposition"] = "attachment; filename=books.csv"
    return response


def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    mail.send(msg)


@flask_app.route('/info')
def info():
    return render_template('info.html')


@flask_app.route('/db_dump')
@login_required
def db_dump_download():
    if current_user.role == ROLE_ADMIN:
        os.environ["PGPASSWORD"] = flask_app.config["DB_PASS"]
        import subprocess
        dir = flask_app.config["BASEDIR"]
        dir_bin = os.path.join(dir, 'bin')
        dir_dumps = os.path.join(dir, 'dumps')

        if not os.path.exists(dir_dumps):
            os.makedirs(dir_dumps)

        # pg_dump		= os.path.join(dir_bin,"pg_dump")
        pg_dump = "pg_dump"
        host = flask_app.config["DB_HOST"]
        db = flask_app.config["DB_NAME"]
        dnow = datetime.datetime.utcnow().strftime("%Y-%m-%d %H%M%S")
        file_name = "{0}@{1} ({2}).backup".format(host, db, dnow)
        full_file_name = os.path.join(dir_dumps, file_name)

        p = subprocess.Popen([pg_dump
                                 , "--host={0}".format(flask_app.config["DB_HOST"])
                                 , "--port={0}".format(flask_app.config["DB_PORT"])
                                 , "--username={0}".format(flask_app.config["DB_USER"])
                                 , "--dbname={0}".format(flask_app.config["DB_NAME"])
                                 , "--no-password"
                                 , "--format=custom"
                                 , "--blobs"
                                 , "--verbose"
                                 , '--file={0}'.format(full_file_name)
                              ]
                             , stdout=subprocess.PIPE, stderr=subprocess.STDOUT
                             )
        # html_rez = "\n<br>".join(p.stdout.readlines())
        log_rez = "".join(p.stdout.readlines())
        p.wait()

        print(log_rez)

        with open(full_file_name, "r") as myfile:
            data = myfile.read()
        os.remove(full_file_name)

        response = flask_app.make_response(data)
        # This is the key: Set the right header for the response
        # to be downloaded, instead of just printed on the browser
        response.headers["Content-Disposition"] = "attachment; filename={0}.dump".format(file_name)
        return response
# /home/buddha3/bin/pg_dump --host ec2-54-197-237-120.compute-1.amazonaws.com --port 5432
# --username "glgurvtsfrusxl" --no-password  --format custom --blobs --verbose
# --file "/home/buddha3/bin/20150314.backup" --dbname="d651rd4mb7oijt"
