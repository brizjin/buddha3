import os

import logging.config
import yaml

config_file_name = os.path.join(os.path.dirname(__file__), "config.yml")
self = globals()
with open(config_file_name, "r") as f:
    config_dict = yaml.load(f.read(), Loader=yaml.FullLoader)
    self.update(config_dict)

logging_config = self.get("LOGGING")
if logging_config:
    logging.config.dictConfig(logging_config)
