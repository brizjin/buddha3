﻿# -*- coding: utf-8 -*-
#APP_CONFIG_PYFILE=../config/windev.py
# DB_URL = SQLALCHEMY_DATABASE_URI = 'postgres://postgres:123@localhost:5432/postgres'
# DB_URL = SQLALCHEMY_DATABASE_URI = 'postgres://postgres:123@192.168.99.100:5432/postgres'
DB_URL = SQLALCHEMY_DATABASE_URI = 'postgres://postgres:123@192.168.99.100:5432/postgres'

SQLALCHEMY_POOL_SIZE = 20
SQLALCHEMY_POOL_TIMEOUT = 20
SQLALCHEMY_RECORD_QUERIES = False
SQLALCHEMY_ECHO = False
SQLALCHEMY_COMMIT_ON_TEARDOWN = False

AMQP_URL = "amqp://ivan:ivan@rmq/"
DEBUG = True  # Если True то не всегда можно попасть по breakpoint
PROFILE = False
# USE_CFFI = True

from datetime import timedelta
from celery.schedules import crontab

# CELERY_IMPORTS		= ("app.views","task",'app.views.subs_pay_for_today','views')

# CELERY_ALWAYS_EAGER = True
DOMEN_ADR = 'http://192.168.59.103:5000'

CELERYBEAT_SCHEDULE = {
    'update_status_every_minute': {
        'task': 'app.views.update_status_created',
        'schedule': timedelta(seconds=60),
    },
    'add-every-monday-morning2': {
        'task': 'app.views.subs_pay_for_today',
        'schedule': crontab(hour=13, minute=40),
    },
}
