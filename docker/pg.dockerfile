FROM postgres

#RUN apt-get update
RUN echo ec2-54-163-227-202.compute-1.amazonaws.com:5432:db3g4eb3jj2je4:ricikzvyfjdyai:ceeaac98a0f1a5c241fca15b7cef7d2395109f9356fd5820841de773c1b7f8be > ~/.pgpass
RUN chmod 0600 ~/.pgpass
#pg_dump --host=<host_name> --port=<port> --username=<username> --password --dbname=<dbname> > output.sql
RUN pg_dump --host=ec2-54-163-227-202.compute-1.amazonaws.com --port=5432 --username=ricikzvyfjdyai --password --dbname=db3g4eb3jj2je4 > output.sql

ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE 5432
CMD ["postgres"]