FROM pypy:2

COPY ./requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt