### Запуск в docker
#### Установка docker toolbox
Устанавливаем docker toolbox, потому, что он может работать с VirtualBox.
А Docker for Windows не может, и работает с Windows Hyper-v, которая глючит (может не везде?)

#### Подготовки для pgadmin в докере
* В virtualbox нужно для вм докера сделать общую папку C:\Users с именем /c/Users
* Затем подключиться к вм и замаунтить папку.  
    ```
    docker-machine ssh
    sudo mount -t vboxsf -o umask=0022,uid=1000,gid=50 /c/Users /c/Users
    ```
    * Папка `/c/User` будет доступна из вм докера
    * Тут `./pg_admin_user_folder` будет разъименован как 
    `/c/Users/BryzzhinIS/Documents/prj/pyprj/buddha3/pg_admin_user_folder` потому,
    что докер композ мы запускаем из папки проекта и она автоматом подставится вместо точки.
    Но файловая система тут уже вм докера, а не хоста. Поэтому нам нужен был маунт выше.
    * При этом сам pgadmin сохранит все свои файлы в папку проекта `pg_admin_user_folder`
    * При сохранении дампа базы он сохранится в `pg_admin_user_folder/storage/user_domain.com/`
    * Логин/пароль для входа указаны в `docker-compose.yaml` как `user@domain.com/123`

#### Старт контейнеров
Из папки проекта нужно выполнить `docker-compose up` для запуска.
Для пересоздания можно также сделать сначала `down` а затем уже `up`

### Дев сервер
#### Адрес postgreSql
Запускаем на питоне 2
Прописываем адрес пострегса в докере в переменную `SQLALCHEMY_DATABASE_URI` в файле `windev.py`.
IP самого докера может быть каждый раз новый. А к базе приложение подключается с хоста по `IP_DOCKER:5432`.
Либо в VirtualBox нужно прокидывать 5432 порт.
#### Запуск
Запускаем скрипкт runserver.py и enviroment variables:
`PYTHONUNBUFFERED=1;APP_CONFIG_PYFILE=../config/windev.py`
#### Адрес девсервера в браузере 
`http://localhost:5000/`
