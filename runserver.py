# from app import flask_app
from app import flask_app
import os
from os import environ

flask_app.config["BASEDIR"] = os.path.abspath(os.path.dirname(__file__))

if __name__ == "__main__":
    # flask_app.run("0.0.0.0", threaded=True)#
    # flask_app.run()
    HOST = environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(environ.get('SERVER_PORT', '5000'))
    except ValueError:
        PORT = "5000"
    flask_app.run(HOST, PORT)
