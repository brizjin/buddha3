# -*- coding: utf-8 -*-
from __future__ import absolute_import
from app import flask_app, db
import app.models as models
import unittest
from app.views.donate import md5str as donate_md5
from app.lib import *


class Md5Class(unittest.TestCase):
    @staticmethod
    def md5str(value):
        return hashlib.md5(value.encode('utf8')).hexdigest()

    def test_md5(self):
        s = 'admin' + '123'
        # print md5str(s)
        self.assertEqual(Md5Class.md5str(s), '0192023a7bbd73250516f069df18b500')
        self.assertEqual(donate_md5(s), Md5Class.md5str(s))

        s = 'admin' + 'Привет'
        # print md5str(s)
        self.assertEqual(Md5Class.md5str(s), '8e2211df57bf2f86530116e85c245e06')
        self.assertEqual(donate_md5(s), Md5Class.md5str(s))

        s = 'admin' + 'Привет'
        # print md5str(s)
        self.assertEqual(md5(s), '8e2211df57bf2f86530116e85c245e06')
        self.assertEqual(md5(s), Md5Class.md5str(s))


class Registration(unittest.TestCase):
    email = 'testuser@gmail.com'

    def setUp(self):
        self.app = flask_app.test_client()

    def tearDown(self):
        for u in db.session.query(models.User).filter_by(email=self.email).all():
            db.session.delete(u)
            db.session.commit()

    def post(self, data):
        return self.app.post('/registration', data=data, follow_redirects=True)

    def test_register(self):
        rv = self.app.get('registration')
        assert u'Регистрация нового пользователя' in rv.data
        rv = self.post(dict(email='', password='', phone=''))
        assert u'Необходимо указать email' in rv.data
        assert u'Необходимо указать телефон' in rv.data
        assert u'Необходимо указать пароль' in rv.data
        rv = self.post(dict(email='ivan@com', password='123', phone='123'))
        assert u'Не верный формат email' in rv.data
        assert u'Не верный формат телефона. Необходимо указать телефон в формате +71234567890' in rv.data
        rv = self.post(dict(email='ivanscvorcov@gmail.com', password='123', phone='123'))
        assert u'Пользователь с email ivanscvorcov@gmail.com уже существует' in rv.data
        rv = self.post(dict(email=self.email, password='123', phone='+79151234567'))
        assert u'Пользователь testuser@gmail.com успешно зарегистрован' in rv.data
        self.assertEqual(len(db.session.query(models.User).filter_by(email=self.email).all()), 1)


class Dumb(unittest.TestCase):
    def dumb_method(self):
        self.assertEqual("1", "2")


class Pages(unittest.TestCase):
    def setUp(self):
        self.app = flask_app.test_client()

        # for u in db.session.query(models.User).filter_by(email='testadmin@gmail.com').all():
        #     db.session.delete(u)
        #     db.session.commit()

        for u in db.session.query(models.User).filter_by(email='testadmin@gmail.com').all():
            db.session.delete(u)
            db.session.commit()

        data = dict(email='testadmin@gmail.com', password='123', phone='+79151234567')
        rv = self.app.post('/registration', data=data, follow_redirects=True)
        assert u'Пользователь testadmin@gmail.com успешно зарегистрован' in rv.data
        u = db.session.query(models.User).filter_by(email='testadmin@gmail.com').first()
        u.role = 1
        # db.session.delete(u)
        db.session.commit()
        print 'SetUp'

    def tearDown(self):
        for u in db.session.query(models.User).filter_by(email='testadmin@gmail.com').all():
            db.session.delete(u)
            db.session.commit()
        print 'tearDown'

    def login(self, email, password):
        return self.app.post('/login', data=dict(email=email, password=password), follow_redirects=True)

    def logout(self):
        return self.app.get('/logout', follow_redirects=True)

    def test_login_logout(self):
        print 'test1'
        assert u'Ошибка e-mail или пароля' in self.login('admin', 'default').data
        assert u'Вы успешно залогинились' in self.login(u'brizjin@gmail.com', u'ghjdf;fk').data
        assert u'Вы успешно разлогинились' in self.logout().data
        assert u'Вы успешно залогинились' in self.login(u'ivanscvorcov@gmail.com', u'ghjdf;fk').data

    def test_mainpages(self):
        print 'test2'
        assert self.app.get('/').status_code == 200
        assert self.app.get('/info').status_code == 200
        assert self.app.get('/logout', follow_redirects=True).status_code == 200
        assert self.app.get('/login').status_code == 200
        assert self.app.get('/registration').status_code == 200
        assert self.app.get('/donate').status_code == 200
        donate = self.app.post('/donate', data=dict(project=1, summa=100), follow_redirects=True)
        assert donate.status_code == 200
        assert u'Перенаправление на страницу оплаты...' in donate.data

    def test_adminpages(self):
        print 'test3'
        assert u'Вы успешно залогинились' in self.login(u'testadmin@gmail.com', u'123').data
        assert self.app.get('/orders').status_code == 200
        assert self.app.get('/projects').status_code == 200
        assert self.app.get('/subs').status_code == 200
        assert self.app.get('/subs_period').status_code == 200
        assert self.app.get('/users').status_code == 200
        assert self.app.get('/stats').status_code == 200
        assert self.app.get('/stats_months').status_code == 200
        assert self.app.get('/sm').status_code == 200
        assert self.app.get('/send_invite').status_code == 200
        assert u'Вы успешно разлогинились' in self.logout().data

    def test_pages(self):
        print 'test4'
        assert u'Вы успешно залогинились' in self.login(u'brizjin@gmail.com', u'ghjdf;fk').data
        assert self.app.get('/my_orders').status_code == 200
        assert self.app.get('/my_subs').status_code == 200
        assert u'Вы успешно разлогинились' in self.logout().data
